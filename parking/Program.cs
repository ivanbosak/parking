﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    class Program
    {
        static void Main(string[] args)
        {
            int action;
            bool parsingResult;

            var parking = Parking.GetInstance();

            while (true)
            {
                Menu.PrintMainHeader();

                parsingResult = Int32.TryParse(Console.ReadLine(), out action);

                if (parsingResult == true)
                {

                    if (action == 1)
                    {
                        while (true)
                        {

                            Menu.PrintCustomerHeader();

                            parsingResult = Int32.TryParse(Console.ReadLine(), out action);

                            if (parsingResult == true)
                            {
                                switch (action)
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        Menu.ParkVehicle(parking);
                                        break;
                                    case 2:
                                        Menu.TekeBackVehicle(parking);
                                        break;
                                    case 3:
                                        Console.WriteLine("On the parking there are {0} free and {1} occupied places.",
                                            parking.GetNumberOfFreePlaces(), parking.GetNumberOfOccupiedPlaces());
                                        Console.ReadLine();
                                        break;
                                    case 4:
                                        Menu.RefillDeposit(parking);
                                        break;
                                    default:
                                        continue;
                                }
                                break;
                            }
                        }
                    }
                    else if (action == 2)
                    {
                        while (true)
                        {

                            Menu.PrintAdministratorHeader();

                            parsingResult = Int32.TryParse(Console.ReadLine(), out action);

                            if (parsingResult == true)
                            {
                                switch (action)
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        Menu.PrintAllVehicles(parking);
                                        break;
                                    case 2:
                                        Menu.PrintTransactionsForMinute(parking);
                                        break;
                                    case 3:
                                        Menu.PrintLogFile(parking);
                                        break;
                                    case 4:
                                        Console.WriteLine("For last minute parking earned - {0}", parking.CalculateEarnedForMinute());
                                        Console.ReadLine();
                                        break;
                                    case 5:
                                        Console.WriteLine("Balance of parking is - {0}", parking.GetBalance());
                                        Console.ReadLine();
                                        break;
                                    default:
                                        continue;
                                }
                                break;
                            }
                        }

                    }
                    Console.Clear();
                }
            }
        }

    }
    
}
