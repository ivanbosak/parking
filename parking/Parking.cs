﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    interface IParking
    {
        IEnumerable<Transaction> GetAllTransactions();
    }
    class Parking
    {
        private static readonly Parking _parking = new Parking();

        private readonly List<Vehicle> _vehicles = new List<Vehicle>();

        private readonly List<Transaction> _transactions = new List<Transaction>();

        private static TransactionHandler _transactionHandler;

        private double _balance;

        protected Parking()
        {
            _balance = Properties.Balance;
            _transactionHandler = new TransactionHandler(new Logger("Transactions.txt"), _vehicles, _transactions);
            _transactionHandler.RunHandler();
        }
        public static Parking GetInstance()
        {
            return _parking;
        }

        public double GetBalance()
        {
            return _balance;
        }

        public void AddBalance(double sum)
        {
            _balance = _balance + sum;
        }

        public int GetNumberOfFreePlaces()
        {
            return Properties.MaxParkingPlaces - _vehicles.Count;
        }
        public int GetNumberOfOccupiedPlaces()
        {
            return _vehicles.Count;
        }
        public int GetNumberOfTransactions()
        {
            return _transactions.Count;
        }
        public string ReadLogFile()
        {
            return _transactionHandler.ReadLog();
        }
        public void AddTransaction(Transaction transaction)
        {
            _transactions.Add(transaction);
        }
        public IEnumerable<Transaction> GetAllTransactions()
        {
            return _transactions;
        }
        public double CalculateEarnedForMinute()
        {
            double sum = 0;

            foreach(Transaction tr in _transactions)
            {
                sum += tr.Withdrawn;
            }
            return sum;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            _vehicles.Add(vehicle);
        }

        public bool RemoveVehicle(Vehicle vehicle)
        {
            return _vehicles.Remove(vehicle);
        }
        public Vehicle FindVehicle(string licensePlates)
        {
            return _vehicles.FirstOrDefault(v => v.LicensePlates
                            .Equals(licensePlates, StringComparison.OrdinalIgnoreCase));
        }
        public IEnumerable<Vehicle> GetAllVehicles()
        {
            return _vehicles;
        }
    }
}
