﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    class Transaction
    {
        public string LicensePlates { get; set; }
        public double Withdrawn { get; set; }
        public Transaction(string licensePlates, double withdrawn)
        {
            LicensePlates = licensePlates;
            Withdrawn = withdrawn;
        }
    }
}
